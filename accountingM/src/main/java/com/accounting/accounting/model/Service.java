package com.accounting.accounting.model;

import lombok.*;

import javax.persistence.*;

@Entity
@ToString
@Table(name = "services")
@NoArgsConstructor
@AllArgsConstructor
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String unitOfMeasurement;
    @Getter
    @Setter
    private Float quantity;
    @Getter
    @Setter
    private Float price;
    @Getter
    @Setter
    private Integer vat;
    @Getter
    @Setter
    private Integer orderNo;



}

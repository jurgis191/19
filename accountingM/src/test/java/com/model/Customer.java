package model;

import lombok.*;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Entity
@Table(name="customers")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    private String companyCode;
    @Getter
    @Setter
    private String vatCode;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String address;
    @Getter
    @Setter
    private String country;
    @Getter
    @Setter
    private String phoneNumber;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String webpage;
    @Getter
    @Setter
    private String addressForCorespondency;
}

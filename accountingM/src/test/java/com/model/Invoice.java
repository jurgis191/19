package model;

import lombok.*;

import javax.persistence.*;

@Entity
@ToString
@Table(name = "invoices")
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    private String number;
    @Getter
    @Setter
    private boolean payed;
    @Getter
    @Setter
    private String comments;
    @Getter
    @Setter
    private Integer term;
    @Getter
    @Setter
    java.sql.Date date;
}

package com.example.cusromers.homework;

import com.homework.xml.customers.Customer;
import com.homework.xml.customers.CustomerDetailsRequest;
import com.homework.xml.customers.CustomerDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class CustomerEndpoint {

    private static final String NAMESPACE_URI = "http://www.homework.com/xml/customers";

    private CustomerRepository customerRepository = new CustomerRepository();

    @Autowired
    public CustomerEndpoint(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CustomerDetailsRequest")
    @ResponsePayload
    public CustomerDetailsResponse getCustomer(@RequestPayload CustomerDetailsRequest request){
        CustomerDetailsResponse response = new CustomerDetailsResponse();
        response.setCustomer(customerRepository.findCustomer(request.getName()));

        return response;
    }
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CustomerDetailsUpdate")
    @ResponsePayload
    public CustomerDetailsResponse updateCustomer(@RequestPayload Customer customer){
        CustomerDetailsResponse response = new CustomerDetailsResponse();
        customerRepository.updateCustomer(customer);
        response.setCustomer(customerRepository.findCustomer(customer.getName()));

        return response;
    }
}

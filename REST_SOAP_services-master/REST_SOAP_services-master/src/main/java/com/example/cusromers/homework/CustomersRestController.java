package com.example.cusromers.homework;

import com.homework.xml.customers.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class CustomersRestController {
    @Autowired CustomerRepository customerRepository;

    @GetMapping("/customer")
    private Map<String, Customer> allCustomers(){
        return customerRepository.findAll();
    }

    @PutMapping("/customer")
    private Customer updateCustomer(@RequestBody Customer customer){
        customerRepository.updateCustomer(customer);
        return customerRepository.findCustomer(customer.getName());
    }

    @PostMapping("/customer")
    private Customer saveCustomer(@RequestBody Customer customer){
        customerRepository.saveCustomer(customer);
        return customerRepository.findCustomer(customer.getName());
    }


    @DeleteMapping("/customer")
    private String saveCustomer(@RequestBody String customerName
                                ,@RequestHeader("name") String name
                                ,@RequestHeader("password") String password
    ){
        if(name.equals("vartotojas")& password.equals("slaptazodis")) {
            customerRepository.deleteCustomer(customerName);
            return customerName;
        }else{return "unoautorized";}
    }
}

package com.example.cusromers.homework;

import com.homework.xml.customers.Customer;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class CustomerRepository {
    private static final Map<String, Customer> customers = new HashMap<>();

    @PostConstruct
    public void initData(){
        Customer customer = new Customer();
        customer.setName("Arminas");
        customer.setNumber(1);
        customer.setAddress("Main str. 25, Vilnius");
        customers.put(customer.getName(), customer);

        customer = new Customer();

        customer.setName("Domantas");
        customer.setNumber(2);
        customer.setAddress("Second str. 15, Kaunas");
        customers.put(customer.getName(), customer);

        customer = new Customer();

        customer.setName("Vejas");
        customer.setNumber(3);
        customer.setAddress("Third str. 25, Klaipeda");
        customers.put(customer.getName(), customer);
    }
    public Customer findCustomer(String name){
        Assert.notNull(name, "Customer name must be filled");
        return customers.get(name);
    }
    public Map<String, Customer> findAll(){
        return customers;
    }
    public Customer updateCustomer(Customer customer){
        Assert.notNull(customer, "Customer must be filled");
        return customers.replace(customer.getName(), customer);
    }
    public Customer deleteCustomer(String name){
        Assert.notNull(name, "Customer must be filled");
        return customers.remove(name);
    }
    public Customer saveCustomer(Customer customer){
        customers.put(customer.getName(),customer);
        return customer;
    }
}

package com.exeptions;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserNameNotFoundException extends RuntimeException {
    public UserNameNotFoundException (String msg){
        super(msg);
    }
    public UserNameNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }
}

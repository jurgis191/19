package com.example.cusromers.homework;

import com.exeptions.UserNameNotFoundException;
import com.homework.xml.customers.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class CustomersRestController {

    @Autowired CustomerRepository customerRepository;

    @GetMapping("/customer")
    private Map<String, Customer> allCustomers(){
        return customerRepository.findAll();
    }

    @PutMapping("/customer")
    private Customer updateCustomer(@RequestBody Customer customer){
        customerRepository.updateCustomer(customer);
        return customerRepository.findCustomer(customer.getName());
    }

    @PostMapping("/customer")
    private Customer saveCustomer(@RequestBody Customer customer){
        customerRepository.saveCustomer(customer);
        return customerRepository.findCustomer(customer.getName());
    }

    @DeleteMapping("/customer")
    private String saveCustomer(@RequestBody(required = false) String customerName){
        if (customerName==null)return "user field is empty";
        try {
            customerRepository.deleteCustomer(customerName);
            return customerName+" deleted";
        }catch (UserNameNotFoundException ex){
            System.out.println(ex);
            return "user didn't founded";
        }

    }
}

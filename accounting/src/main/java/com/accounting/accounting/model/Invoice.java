package com.accounting.accounting.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@ToString
@Table(name = "invoices")
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    private String number;
    @Getter
    @Setter
    private boolean payed;
    @Getter
    @Setter
    private String comments;
    @Getter
    @Setter
    private Integer term;
    @Getter
    @Setter
    java.sql.Date date;
    @OneToMany(
            cascade=CascadeType.ALL
            ,fetch=FetchType.LAZY
    )
    @JoinColumn(name="invoices_id")
    @JsonManagedReference
    @Getter
    @Setter
    private List<Service> services;

    @ManyToOne(
            fetch=FetchType.LAZY
    )
    @JoinColumn(name="customers_id")
    @JsonBackReference
    private Customer customer;
}

package com.accounting.accounting.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="customers")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    private String companyCode;
    @Getter
    @Setter
    private String vatCode;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String address;
    @Getter
    @Setter
    private String country;
    @Getter
    @Setter
    private String phoneNumber;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String webpage;
    @Getter
    @Setter
    private String addressForCodependency;
    @OneToMany(
            cascade=CascadeType.ALL
            ,fetch=FetchType.LAZY
    )
    @JoinColumn(name="customers_id")
    @JsonManagedReference
    private List<Invoice> invoices;
}

package com.accounting.accounting.controller;

import com.accounting.accounting.model.Invoice;
import com.accounting.accounting.model.Service;
import com.accounting.accounting.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class InvoiceController {
    @Autowired
    InvoiceRepository invoiceRepository;
    @GetMapping("/mok")
    private Invoice fillUp(){

        Invoice invoice = new Invoice();
        Service service = new Service();
        Service service1 = new Service();
        service.setName("cleaning");
        service.setPrice(3.5f);
        service.setQuantity(2f);
        service1.setName("repairing");
        service1.setPrice(5.5f);
        service1.setQuantity(4f);
        List<Service> services = new ArrayList<>();
        services.add(service);
        services.add(service1);
        invoice.setNumber("1");
        invoice.setServices(services);
        invoiceRepository.save(invoice);

        return invoice;
    }

    @PostMapping("/save")
    private Invoice saveInvoice(@RequestBody Invoice invoice){
        invoiceRepository.save(invoice);
        return invoice;
    }

    @GetMapping("/all")
    private Iterable<Invoice> getAll(){
        return invoiceRepository.findAll();
    }
}

package com.homework.soeprest;

import com.sun.xml.internal.ws.util.StringUtils;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Validator {

    static Map<String, Integer> cuntries = new HashMap<String, Integer>() {
        {
            put("LT", 20);
            put("LV", 21);
            put("EE", 20);
            put("PL", 28);
        }
    };

    public boolean ibanCheck (String iban){

        System.out.println(iban);

        int result = 0;

        String prefix = iban.substring(0,2).toUpperCase();

        if(prefixCheck(prefix))
            if (iban.length() != lengthFinder(prefix)){
                System.out.println("netinkamas saskaitos numerio ilgis");
            }else{
                System.out.println("saskaitos numerio ilgis tinkamas");
                result=ibanCheckSum(iban);
            }
            return result == 1;
    }

    public int ibanCheckSum ( String iban){
        String ibanModified = "" + iban.substring(4) + charConverter(iban.substring(0,2)) + iban.substring(2,4);

        try
        {
            BigInteger ibanInt = new BigInteger(ibanModified);
            BigInteger dal = new BigInteger("97");
            if (ibanInt.mod(dal).compareTo(BigInteger.ONE)==0){
                System.out.println("saskaitos numerio kontroline suma tinkama");
                return 1;
            }else{
                System.out.println("netinkama saskaitos numerio kontroline suma");
                return -1;
            }
        }
        catch(NumberFormatException nfe)
        {
            System.out.println("neimanoma apskaiciuoti saskaitos numerio kontrolines sumos");
            return -1;
        }
    }

    public void fileCheck (String url){

        String urlOut = ""+url.substring(0,(url.length()-3))+"out";
        File file = new File(url);
        if(file.exists())
        try{
            Scanner input = new Scanner(file);
            PrintWriter writer = new PrintWriter( urlOut, "UTF-8");
            while (input.hasNext()) {
                String iban=input.next();
                writer.write(iban + ibanCheck(iban));
                writer.write(System.getProperty( "line.separator" ));
            }
            input.close();
            writer.close();
            System.out.println();
            System.out.println("irasyta i faila: " + urlOut);
        }
        catch(Exception e){}
        else
            System.out.println("nerastas failas");
    }

    private String charConverter (String prefix){
        StringBuilder converted = new StringBuilder(2);
        for (int i = 0; i < prefix.length(); i++)
            converted.append(Character.toUpperCase(prefix.charAt(i))-55);
        return converted.toString();

    }
    public static Integer lengthFinder(String prefix) {
        return cuntries.get(prefix);
    }
    static boolean prefixCheck(String prefix){
        for(String set:cuntries.keySet())
            if(prefix.equalsIgnoreCase(set)) {
                System.out.println("salies kodas sekmingai rastas");
                return true;
            }
        System.out.println("nepavyko rasti salies kodo");
        return false;
    }



}

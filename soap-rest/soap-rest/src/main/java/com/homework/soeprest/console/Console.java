package com.homework.soeprest;

import com.homework.soeprest.config.Config;import java.util.Scanner;

public class Console {
    
    public static void startConsole() {
        
        Config.Validator validator1=new Config.Validator();
        Config.Validator validator = new Config.Validator();
        System.out.println("Pasirinkite uzduoti:");
        System.out.println("1 - Interaktyvus IBAN numerių tikrinimas");
        System.out.println("2 - IBAN numerių iš tekstinio failo tikrinimas");
        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        switch(choice){
            case 1:
                System.out.println("Iveskite saskaitos numeri: ");
                validator.ibanCheck(in.next());
                break;
            case 2:
                System.out.println("iveskite kelia iki vailo bei jo pavadinima ");
                String url = in.next();
                validator.fileCheck(url);
                break;
            default:
                System.out.println("neteisingas pasirinkimas");
        }
    }

    
}
